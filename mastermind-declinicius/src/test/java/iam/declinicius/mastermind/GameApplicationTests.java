package iam.declinicius.mastermind;

import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import iam.declinicius.mastermind.base.Game;
import iam.declinicius.mastermind.base.GameAttempt;
import iam.declinicius.mastermind.dto.GameResult;
import iam.declinicius.mastermind.exceptions.MMException;
import iam.declinicius.mastermind.main.GameApplication;
import iam.declinicius.mastermind.repositories.AttemptRepository;
import iam.declinicius.mastermind.repositories.GameRepository;
import iam.declinicius.mastermind.rest.GameController;
import iam.declinicius.mastermind.services.IGameService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GameApplication.class)
public class GameApplicationTests {
	
	private final static String USER_NAME = "Pere Galceran";
	private Long gameId = 1L;
	private String gameSolution = "";
	
	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();
	
	@Autowired
	private IGameService gameService;
	
    @Autowired
    private GameController gameController;
    
    @Autowired
    private GameRepository gameRepository;
    
    @Autowired
    private AttemptRepository attemptRepository;
    
    
    @Test
    public void controllerOK() {
    	assertThat(gameController).isNotNull();
    }
    
    @Test
    public void serviceOK() {
    	assertThat(gameService).isNotNull();
    }
    
    @Test
    public void repositoryOK() {
    	assertThat(gameRepository).isNotNull();
    	assertThat(attemptRepository).isNotNull();
    }
    
    @Test
    public void createAndCheckGame() throws MMException{
    	Game game = gameService.createGame(USER_NAME);
    	assertThat(game).isNotNull();
    	gameId = game.getId();
    	gameSolution = game.getSolution();
    	
    	//repeatedGame to check Get from repository
    	Game repeatedGame = gameService.getGame(gameId);
    	assertEquals(gameSolution, repeatedGame.getSolution());
    	
    	//checkGameSolution 0 blacks 0 whites 
    	GameResult gr = gameService.checkUserSolution(gameId, USER_NAME, "DDDD");
    	assertTrue(gr.getAttempts()==1);
    	assertTrue(gr.getBlacks()==0);
    	assertTrue(gr.getWhites()==0);
    	
    	//check round 2, whites or maybe blacks 
    	GameResult gr2 = gameService.checkUserSolution(gameId, USER_NAME, new StringBuilder(gameSolution).reverse().toString());
    	assertTrue(gr2.getAttempts()==2);
    	GameResult gr3 = gameService.checkUserSolution(gameId, USER_NAME, gameSolution);
    	assertTrue(gr3.getAttempts()==3);
    	assertTrue(gr3.getBlacks()==4);
    	assertTrue(gr3.getWhites()==0);
    	
    	//test history
    	List<GameAttempt> gameAttempts = gameService.getGameHistory(gameId);
    	assertTrue(gameAttempts.size() == 3);
    	
    	//removeGame
    	gameRepository.delete(repeatedGame);
    }
    	
    @Test
    public void exceptionThrownOK() throws MMException{
    	exceptionRule.expect(MMException.class);
    	exceptionRule.expectMessage(format("Cannot find game with %s",gameId));
    	gameService.getGame(gameId);
    }
    
    
}
