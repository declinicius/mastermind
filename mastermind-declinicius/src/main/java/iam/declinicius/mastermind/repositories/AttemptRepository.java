package iam.declinicius.mastermind.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import iam.declinicius.mastermind.base.GameAttempt;

@Repository
public interface AttemptRepository extends JpaRepository<GameAttempt, Long> {

	List<GameAttempt> findAllAttemptsById(Long gameId);
	
}
