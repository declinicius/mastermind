package iam.declinicius.mastermind.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import iam.declinicius.mastermind.base.Game;

@Repository
public interface GameRepository extends CrudRepository<Game, Long>  {

}
