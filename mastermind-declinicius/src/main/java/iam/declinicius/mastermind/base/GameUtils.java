package iam.declinicius.mastermind.base;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import iam.declinicius.mastermind.dto.GameRequestDto;
import iam.declinicius.mastermind.dto.GameResult;
import iam.declinicius.mastermind.exceptions.MMException;


public class GameUtils {
	
	private static int SOLUTION_SIZE = 4; 
	
	public enum Colors {
		  G {
		      public String toString() {
		          return "G";
		      }},
		  B {
		      public String toString() {
		          return "B";
		      }},
		  Y {
		      public String toString() {
		          return "Y";
		      }},
		  R {
		      public String toString() {
		          return "R";
		      }},
		  O {
		      public String toString() {
		          return "O";
		      }},
		  W {
		      public String toString() {
		          return "W";
		      }}
	}
	
	private static final List<Colors> COLOR_VALUES =
		    Collections.unmodifiableList(Arrays.asList(Colors.values()));
	private static final String SPLIT_EVERY_CHARACTER = "(?!^)";
	
	public static int COLORS_SIZE = Colors.values().length;
		 
	/**
	 * Generates new Solution for new Game
	 * @param colors - <code>Colors</code> from enumeration
	 * @return <code>String</code> - parsed columns
	 */
	public static String generateNewGameSolution() {
		String solution = "";
		
		for(int i=0;i<SOLUTION_SIZE;i++) {
			Random RANDOM = new Random();
			solution+=COLOR_VALUES.get(RANDOM.nextInt(COLORS_SIZE));
		}
		
		return solution;
	}
	
	/**
	 * 
	 * @param gameSolution - <code>String</code> containing the game solution
	 * @param userSolution - <code>String</code> containing the user attempt solution
	 * @param gameAttempts 
	 * @return
	 * @throws MMException
	 */
	public static GameResult checkSolution(String gameSolution, String userSolution, List<GameAttempt> gameAttempts) throws MMException{
		if(gameSolution.length()!=userSolution.length()) {
			throw new MMException("The user solution has different size than the original game");
		}
		GameResult gr = new GameResult();
		gr.setSolved(gameSolution.equals(userSolution));
		gr.setBlacks(checkBlacks(gameSolution, userSolution));
		gr.setWhites(checkWhites(gameSolution,userSolution,gr.getBlacks()));
		gr.setAttempts(gameAttempts.size());
		return gr;
	}
	
	
	/**
	 * Returns the exact matches about the user attempt
	 * @param gameSolution - <code>String</code> containing the game solution
	 * @param userSolution - <code>String</code> containing the user attempt solution
	 * @return <code>int</code> number of matching positions 
	 */
	private static int checkBlacks(String gameSolution, String userSolution) {
		int exact = 0;
		for(int i=0;i<gameSolution.length();i++) {
			if(gameSolution.charAt(i)==(userSolution.charAt(i))) {
				exact++;
			}
		}
		return exact;
	}
	
	/**
	 * 
	 * @param gameSolution
	 * @param userSolution
	 * @param exact
	 * @return
	 */
	private static int checkWhites(String gameSolution, String userSolution, int exact) {
		int exists = 0;
		String auxSolution = new String(userSolution);
		String[] attempt = gameSolution.split(SPLIT_EVERY_CHARACTER);
		for(int i=0;i<attempt.length;i++) {
			int pos = auxSolution.indexOf(attempt[i]);
			if(pos>-1) {
				auxSolution = new StringBuilder(auxSolution).deleteCharAt(pos).toString();
				exists++;
			}
		}
		return exists-exact;
	}
	
	public static void valideteNewGameRequest(GameRequestDto gameRequest) throws MMException{
		//TO-DO validate userName in new Request
		
	}
}
