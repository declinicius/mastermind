package iam.declinicius.mastermind.base;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name="GameAttempt", schema="MasterMind")
@NamedQuery(name="GameAttempt.findAllAttemptsById", query="SELECT ga FROM GameAttempt ga where ga.game.id = :gameId order by ga.attemptTime desc") 
public class GameAttempt implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue
    private long attempt_id;
	
	@ManyToOne
	@JoinColumn(name = "id")
	private Game game;
	
	@Column
	private String userSolution;
	
	@CreatedDate
	@Column
	private Date attemptTime;

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public String getUserSolution() {
		return userSolution;
	}

	public void setUserSolution(String userSolution) {
		this.userSolution = userSolution;
	}

	public Date getAttemptTime() {
		return attemptTime;
	}

	public void setAttemptTime(Date attemptTime) {
		this.attemptTime = attemptTime;
	}
	
	
	
}
