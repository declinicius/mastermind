package iam.declinicius.mastermind.services;

import static java.lang.String.format;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import iam.declinicius.mastermind.base.Game;
import iam.declinicius.mastermind.base.GameAttempt;
import iam.declinicius.mastermind.base.GameUtils;
import iam.declinicius.mastermind.dto.GameResult;
import iam.declinicius.mastermind.exceptions.MMException;
import iam.declinicius.mastermind.repositories.AttemptRepository;
import iam.declinicius.mastermind.repositories.GameRepository;

@Service
public class GameService implements IGameService{
	
	private static final Logger log = LoggerFactory.getLogger(GameService.class);
	private static final String GAME_DESCRIPTION = "Try to find the correct answer using the given Game 'possibilities'!";
	/**
	 * This will help us to store content on db
	 */
	@Autowired
	private transient GameRepository gameRepository;
	
	@Autowired
	private transient AttemptRepository attemptRepository;

	/**
	 * 
	 * @param userName
	 * @param showSolution
	 * @return <code>Game</code>
	 */
	@Override
	public Game createGame(String userName) {
		Game g = new Game();
		g.setUserName(userName);
		g.setSolution(GameUtils.generateNewGameSolution());
		g.setDescription(GAME_DESCRIPTION);
		g.setAttempts(null);
		g.setCreationDate(new Date());
		log.debug(format("User '%s' requests new game (its solution is: '%s')", userName, g.getSolution()));
		return gameRepository.save(g);
	}
	
	/**
	 * 
	 * @param gameId
	 * @param userName
	 * @param userSolution
	 * @return
	 * @throws MMException
	 */
	@Override
	public GameResult checkUserSolution(Long gameId, String userName, String userSolution) throws MMException{
			Game game = getGame(gameId);
			GameAttempt ga = new GameAttempt();
			ga.setGame(game);
			ga.setUserSolution(userSolution);
			ga.setAttemptTime(new Date());
			attemptRepository.save(ga);
			return GameUtils.checkSolution(game.getSolution(),userSolution,attemptRepository.findAllAttemptsById(gameId));
	}
	
	/**
	 * Retrieves the game from the DB using gameId
	 * @param gameId
	 * @return <code>Game</code>
	 * @throws MMException
	 */
	@Override
	public Game getGame(Long gameId) throws MMException  {
		 Optional<Game> opt = gameRepository.findById(gameId);
		if(!opt.isPresent()) {
			throw new MMException(format("Cannot find game with %s",gameId));
		}
		Game game = opt.get();
		return game;
	}
	
	/**
	 * 
	 * @param gameId
	 * @return
	 * @throws MMException
	 */
	@Override
	public List<GameAttempt> getGameHistory(Long gameId) throws MMException {
		return attemptRepository.findAllAttemptsById(gameId);
	}
	
}
