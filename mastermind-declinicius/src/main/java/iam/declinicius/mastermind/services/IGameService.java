package iam.declinicius.mastermind.services;

import java.util.List;

import iam.declinicius.mastermind.base.Game;
import iam.declinicius.mastermind.base.GameAttempt;
import iam.declinicius.mastermind.dto.GameResult;
import iam.declinicius.mastermind.exceptions.MMException;

public interface IGameService {

	Game createGame(String userName);

	GameResult checkUserSolution(Long gameId, String userName, String userSolution) throws MMException;

	Game getGame(Long gameId) throws MMException;

	List<GameAttempt> getGameHistory(Long gameId) throws MMException;

}
