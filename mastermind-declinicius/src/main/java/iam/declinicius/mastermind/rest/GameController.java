package iam.declinicius.mastermind.rest;

import static java.lang.String.format;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import iam.declinicius.mastermind.base.GameUtils;
import iam.declinicius.mastermind.dto.GameRequestDto;
import iam.declinicius.mastermind.dto.GameResponseDto;
import iam.declinicius.mastermind.dto.helpers.EntityToDto;
import iam.declinicius.mastermind.exceptions.MMException;
import iam.declinicius.mastermind.services.IGameService;
import io.swagger.annotations.Api;
/**
 * GameController - REST API responding methods:
 * \/new\/game - Creates New Game
 * \/game\/{id}\/attempt - Attempts new Solution by game {id}
 * \/game\/{id}\/history - Shows History of Game Attempts
 * @author pere.galceran
 *
 */
@Controller
@RestController(value="gameController")
@Api(value="GameController", description="Allows interaction with MasterMind Game")
public class GameController {
	
	private static final Logger log = LoggerFactory.getLogger(GameController.class);
	
	@Autowired
	private transient IGameService gameService;
	
	/**
	 * EndPoint - /game/new
	 * @param gameRequest - application/json object may contain "userName"
	 * @param response - <code>HttpServletResponse</code>
	 * @return - <code>GameRequestDto</code> - Game Configuration
	 * @throws IOException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	@RequestMapping(value = "/game/new", method = RequestMethod.POST)
	public ResponseEntity<GameResponseDto> newGame(
			@RequestBody final GameRequestDto gameRequest, final HttpServletResponse response ) throws IOException {
		try {
			GameUtils.valideteNewGameRequest(gameRequest);
			GameResponseDto result = EntityToDto.gameToDto(gameService.createGame(gameRequest.getUserName()),false);
			log.debug(format("User '%s' requests new game (its id: '%s')", gameRequest.getUserName(), result.getId()));
			return new ResponseEntity<>(result, HttpStatus.OK);
		}catch(MMException mme) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, mme.getCause().toString());
			return null;
		}
	}
	
	/**
	 * EndPoint - /game/{id}/attempt
	 * @param gameId 
	 * @param gameRequest
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	@RequestMapping(value = "/game/{id}/attempt", method = RequestMethod.POST)
	public ResponseEntity<GameResponseDto> checkUserResult(@PathVariable("id") Long gameId, 
			@RequestBody final GameRequestDto gameRequest, final HttpServletResponse response ) throws IOException {
		try {
			GameResponseDto result = EntityToDto.gameResultToDto(gameService.checkUserSolution(gameId, gameRequest.getUserName(), gameRequest.getUserAttempt()),gameId.toString());
			result.setAttempts(EntityToDto.gameAttemptListToDto(gameService.getGameHistory(gameId)));
			log.debug(format("User '%s' requests new game (its id: '%s')", gameRequest.getUserName(), result.getId()));
			return new ResponseEntity<>(result, HttpStatus.OK);
		}catch(MMException mme) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, mme.getMessage());
			return null;
		}
	}
	
	
	/**
	 * EndPoint - /game/{id}/history
	 * @param gameId 
	 * @param gameRequest
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	@RequestMapping(value = "/game/{id}/history", method = RequestMethod.POST)
	public ResponseEntity<GameResponseDto> gameHistory(@PathVariable("id") Long gameId, 
			@RequestBody final GameRequestDto gameRequest, final HttpServletResponse response ) throws IOException {
		try {
			GameResponseDto result = EntityToDto.gameHistoryToDto(gameService.getGameHistory(gameId),gameId.toString());
			result.setAttempts(EntityToDto.gameAttemptListToDto(gameService.getGameHistory(gameId)));
			result.setGameResult(null);
			log.debug(format("User '%s' requests game history of id: '%s')", gameRequest.getUserName(), result.getId()));
			return new ResponseEntity<>(result, HttpStatus.OK);
		}catch(MMException mme) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, mme.getMessage());
			return null;
		}
	}
	
	/**
	 * 
	 * @param gameId
	 * @param gameRequest
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = false)
	@RequestMapping(value = "/game/{id}/solution", method = RequestMethod.POST)
	public ResponseEntity<GameResponseDto> showGameSolution(@PathVariable("id") Long gameId, 
			@RequestBody final GameRequestDto gameRequest, final HttpServletResponse response ) throws IOException {
		try {
			GameResponseDto result = EntityToDto.gameToDto(gameService.getGame(gameId),true);
			log.debug(format("User '%s' requests new game (its id: '%s')", gameRequest.getUserName(), result.getId()));
			return new ResponseEntity<>(result, HttpStatus.OK);
		}catch(MMException mme) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, mme.getMessage());
			return null;
		}
	}

}
