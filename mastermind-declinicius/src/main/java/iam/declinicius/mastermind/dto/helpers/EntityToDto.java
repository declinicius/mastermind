package iam.declinicius.mastermind.dto.helpers;

import java.util.ArrayList;
import java.util.List;

import iam.declinicius.mastermind.base.Game;
import iam.declinicius.mastermind.base.GameAttempt;
import iam.declinicius.mastermind.base.GameUtils;
import iam.declinicius.mastermind.dto.AttemptDto;
import iam.declinicius.mastermind.dto.GameResponseDto;
import iam.declinicius.mastermind.dto.GameResult;

public class EntityToDto {
	
	public static final String MASTERMIND = "MasterMind";
	private static final String SOLUTION = "????";
	/**
	 * Transforms the Entity <code>Game</code> object, with the desired response
	 * @param game -- <code>Game</code>
	 * @return <code>GameResponseDto</code>
	 */
	public static GameResponseDto gameToDto(Game game, boolean showSolution) {
		GameResponseDto grd = new GameResponseDto();
		grd.setId(game.getId().toString());
		
		if(showSolution)
			grd.setGameKey(game.getSolution());
		else
			grd.setGameKey(SOLUTION);
		
		grd.setGameResult(new GameResult());
		grd.setPossibilities(GameUtils.Colors.values());
		grd.setCurrentGame(MASTERMIND);
		return grd;
	}
	
	/**
	 * Transforms the GameAttempt to <code>GameResponseDto</code>
	 * @param gameResult
	 * @param gameId
	 * @return <code>GameResponseDto</code>
	 */
	public static GameResponseDto gameResultToDto(GameResult gameResult, String gameId) {
		GameResponseDto grd = new GameResponseDto();
		grd.setId(gameId);
		grd.setGameResult(gameResult);
		grd.setPossibilities(GameUtils.Colors.values());
		return grd;
	}
	
	/**
	 * Transforms list of <code>GameAttempt</code> to <code>GameResponseDto</code>
	 * @param gameHistory
	 * @param gameId
	 * @return <code>GameResponseDto</code>
	 */
	public static GameResponseDto gameHistoryToDto(List<GameAttempt> gameHistory, String gameId) {
		GameResponseDto grd = new GameResponseDto();
		grd.setHistory(gameAttemptListToDto(gameHistory));
		grd.setId(gameId);
		return grd;
	}
	
	/**
	 * Helper to transform list of GameAttempt to <code>Attempt</code> objects
	 * @param gameHistory
	 * @return <code>List\<<code>AttemptDto</code>\></code>
	 */
	public static List<AttemptDto> gameAttemptListToDto(List<GameAttempt> gameHistory) {
		List<AttemptDto> attempts = new ArrayList<AttemptDto>();
		for(GameAttempt ga : gameHistory) {
			attempts.add(new AttemptDto(ga.getUserSolution(),ga.getAttemptTime()));
		}
		return attempts;
	}


}
