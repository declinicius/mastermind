package iam.declinicius.mastermind.dto;

import java.io.Serializable;
import java.util.List;

import iam.declinicius.mastermind.base.GameUtils.Colors;

/**
 * Common Response to MasterMind Game
 * @author pere.galceran
 *
 */
public class GameResponseDto implements Serializable {

	private static final long serialVersionUID = 7605014113646648600L;
	
	private String id;
	private String gameKey;
	private Colors[] possibilities;
	private String currentGame;
	private GameResult gameResult;

	private List<AttemptDto> attempts;
		
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGameKey() {
		return gameKey;
	}
	public void setGameKey(String gameKey) {
		this.gameKey = gameKey;
	}
	public GameResult getGameResult() {
		return gameResult;
	}
	public void setGameResult(GameResult gameResult) {
		this.gameResult = gameResult;
	}
	public Colors[] getPossibilities() {
		return possibilities;
	}
	public void setPossibilities(Colors[] possibilities) {
		this.possibilities = possibilities;
	}
	public String getCurrentGame() {
		return currentGame;
	}
	public void setCurrentGame(String currentGame) {
		this.currentGame = currentGame;
	}
	public void setHistory(List<AttemptDto> attempts) {
		this.attempts = attempts;
	}
	public List<AttemptDto> getAttempts() {
		return attempts;
	}
	public void setAttempts(List<AttemptDto> attempts) {
		this.attempts = attempts;
	} 

}
