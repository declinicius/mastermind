package iam.declinicius.mastermind.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GameRequestDto implements Serializable{
	
	private static final long serialVersionUID = 4353089774426846082L;
	
	@JsonProperty(value = "userName")
	private String userName;
	@JsonProperty(value = "userAttempt")
	private String userAttempt;
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserAttempt() {
		return userAttempt;
	}

	public void setUserAttempt(String userAttempt) {
		this.userAttempt = userAttempt;
	}
}
