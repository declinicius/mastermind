package iam.declinicius.mastermind.dto;

import java.io.Serializable;

public class GameResult implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean solved;
	private int attempts;
	private int whites;
	private int blacks;
	
	public GameResult() {
		super();
		this.solved = false;
		this.attempts = 0;
		this.whites = 0;
		this.blacks = 0;
	}

	public boolean isSolved() {
		return solved;
	}
	
	public void setSolved(boolean solved) {
		this.solved = solved;
	}
	
	public int getAttempts() {
		return attempts;
	}
	
	public void setAttempts(int attempts) {
		this.attempts = attempts;
	}
	
	public int getWhites() {
		return whites;
	}
	
	public void setWhites(int exist) {
		this.whites = exist;
	}
	
	public int getBlacks() {
		return blacks;
	}
	
	public void setBlacks(int exact) {
		this.blacks = exact;
	}

}
