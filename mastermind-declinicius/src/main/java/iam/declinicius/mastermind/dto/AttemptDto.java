package iam.declinicius.mastermind.dto;

import java.io.Serializable;
import java.util.Date;


public class AttemptDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userSolution;
	private Date requestDate;
	public AttemptDto(String userSolution, Date attemptTime) {
		this.userSolution = userSolution;
		this.requestDate = attemptTime;
	}
	
	public String getUserSolution() {
		return userSolution;
	}

	public void setUserSolution(String userSolution) {
		this.userSolution = userSolution;
	}

	public Date getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(Date requestDate) {
		this.requestDate = requestDate;
	}
	
	
}
