package iam.declinicius.mastermind.main;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {  
	
	
	private ApiInfo apiInfo() {
	     return new ApiInfo(
	       "MasterMind Api", 
	       "by @declinicius.", 
	       "API TOS", 
	       "You must win the game", 
	       new Contact("Pere Galceran", "https://bitbucket.org/declinicius/mastermind/", "declinicius@gmail.com"), 
	       "License of API", "https://bitbucket.org/declinicius/mastermind/src/master/README.md", Collections.emptyList());
	}
	
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.regex("^.*\\b(game)\\b.*$"))                          
          .build()
          .apiInfo(apiInfo());                                           
    }
}
