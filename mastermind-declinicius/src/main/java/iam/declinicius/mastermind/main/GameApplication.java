package iam.declinicius.mastermind.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = { "iam.declinicius.mastermind"})
@ComponentScan("iam.declinicius.mastermind")
@EntityScan("iam.declinicius.mastermind.base")
@EnableJpaRepositories("iam.declinicius.mastermind.repositories")
public class GameApplication {

	public static void main(String[] args) {
		SpringApplication.run(GameApplication.class, args);
	}
}
