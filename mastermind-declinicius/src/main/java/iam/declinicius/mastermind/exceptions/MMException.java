package iam.declinicius.mastermind.exceptions;

public class MMException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public MMException(String  theException) {
		super(theException);
	}

}
