 _ _ _                      _ _ _              
' ) ) )          _/_       ' ) ) )            /
 / / /  __.  _   /   _  __  / / /  o ____  __/ 
/ ' (_ (_/|_/_)_<__ </_/ (_/ ' (_ <_/ / <_(_/_ 
                                               
..................... by Pere Galceran                                  
#####################################################################

Installation details:

JDK 1.8: download at https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

IDE SpringToolSuite 4.4.0 : download at : https://spring.io/tools

Git:            download project into your repository Git:
				git clone https://declinicius@bitbucket.org/declinicius/mastermind.git
				Import your project on your SpringToolSuite (or Eclipse) workspace

Maven:          Depending on your IDE version, it will not detect its project Maven Nature. Help-> https://maven.apache.org/
                "Right Click" on the project view over "mastermind-declinicius" (Configure -> Add Maven Nature )
                Remember to Update your dependencies (alt+f5)

SpringBoot & JPA 2.0.5:	 https://spring.io/guides/gs/spring-boot/
		-  Maven reading pom.xml dependencies definitions will resolve your dependencies.

MySQL:    - edit application.properties to use your own db
			- the db will be auto-generated due to configuration, but you need to create the schema first.
			- the db must be running when you run the project
			- spring.jpa.hibernate.ddl-auto = create
            - it's working on "create" mode - remember to change to "update" mode on this file


Once you get ready to run, install project using m2 runner. You may need a JDK to compile the project and get the executable jar file on the target folder.

run it on terminal with:
		"java -jar mastermind-backend-declinicius-1.0.0.jar"
		 
		# by default its port is 8080, but you can change it on application.properties file

Swagger2:  https://editor.swagger.io/
	Will help you showing the api-docs specification. 

The best way to try the endpoints, is getting the swagger2 json specification, it will be running with the application on this url : "http://localhost:8080/v2/api-docs" 
          (f.e.: https://editor.swagger.io/ may help you to use the api Rest) 

Postman: https://www.getpostman.com/
Other good choice will be copying the swagger json content on PostMan application to run the tests.  

Application:

There is a GameController working on 4 endpoints:

1.New Game:
	path: "/game/new"
	Description: generates new game
	Method: Post
	ContentType: application/json
	Consume:
	{
	  "userName": "string"
	}
	
	Returns:
	{
    "id": "1",
    "gameKey": null, /* secret key to solve the game */
    "possibilities": [ /* the string to solve and play the game has 4 positions length, there are 4 of these characters containing a string of 4, may be repeated characters */
        "G",
        "B",
        "Y",
        "R",
        "O",
        "W"
    ],
    "currentGame": "MasterMind",
	    "gameResult": {
	        "solved": false,
	        "attempts": 0,
	        "whites": 0,	/**match, but not in the correct position**/
	        "blacks": 0    /**exact match**/
	    },
	    "attempts": null
	}
	
2.Game Attempt:
	path: "/game/{id}/attempt" - the id is needed on db, otherwise the query will crash
	Description: user tries to guess the secret key
	Method: Post
	ContentType: application/json
	Consume:
	{
	  "userAttempt": "WBGR", 
	  "userName": "Pere Galceran" 
	}
	/* userAttempt: 4 characters */
	/* userName : current userName */

3.Game History:
	path: "/game/{id}/history" - the id is needed on db, otherwise the query will crash
	Description: returns all user attempts
	Method: Post
	ContentType: application/json
	Consume:
	{
	  "userName": "Pere Galceran" 
	}
	userName : /* current userName */

4.Game Solution:
	path: "game/:id/solution" - the id is needed on db, otherwise the query will crash
	Description: returns the given id game solution
	Method: Post
	ContentType: application/json
	Consume:
	{
	  "userName": "Pere Galceran" 
	}
	userName : /* current userName */
 		 		
	
Again, the best way to watch it is running the server. Going to "http://localhost:8080/v2/api-docs" and copy that content on the left side of https://editor.swagger.io/

if you cannot run it, I posted here to show yourself how it works:
{"swagger":"2.0","info":{"description":"by @declinicius.","version":"API TOS","title":"MasterMind Api","termsOfService":"You must win the game","contact":{"name":"Pere Galceran","url":"https://bitbucket.org/declinicius/mastermind/","email":"declinicius@gmail.com"},"license":{"name":"License of API","url":"https://bitbucket.org/declinicius/mastermind/src/master/README.md"}},"host":"localhost:8080","basePath":"/","tags":[{"name":"game-controller","description":"Allows interaction with MasterMind Game"}],"paths":{"/game/new":{"post":{"tags":["game-controller"],"summary":"newGame","operationId":"newGameUsingPOST","consumes":["application/json"],"produces":["*/*"],"parameters":[{"in":"body","name":"gameRequest","description":"gameRequest","required":true,"schema":{"$ref":"#/definitions/GameRequestDto"}}],"responses":{"200":{"description":"OK","schema":{"$ref":"#/definitions/GameResponseDto"}},"201":{"description":"Created"},"401":{"description":"Unauthorized"},"403":{"description":"Forbidden"},"404":{"description":"Not Found"}},"deprecated":false}},"/game/{id}/attempt":{"post":{"tags":["game-controller"],"summary":"checkUserResult","operationId":"checkUserResultUsingPOST","consumes":["application/json"],"produces":["*/*"],"parameters":[{"in":"body","name":"gameRequest","description":"gameRequest","required":true,"schema":{"$ref":"#/definitions/GameRequestDto"}},{"name":"id","in":"path","description":"id","required":true,"type":"integer","format":"int64"}],"responses":{"200":{"description":"OK","schema":{"$ref":"#/definitions/GameResponseDto"}},"201":{"description":"Created"},"401":{"description":"Unauthorized"},"403":{"description":"Forbidden"},"404":{"description":"Not Found"}},"deprecated":false}},"/game/{id}/history":{"post":{"tags":["game-controller"],"summary":"gameHistory","operationId":"gameHistoryUsingPOST","consumes":["application/json"],"produces":["*/*"],"parameters":[{"in":"body","name":"gameRequest","description":"gameRequest","required":true,"schema":{"$ref":"#/definitions/GameRequestDto"}},{"name":"id","in":"path","description":"id","required":true,"type":"integer","format":"int64"}],"responses":{"200":{"description":"OK","schema":{"$ref":"#/definitions/GameResponseDto"}},"201":{"description":"Created"},"401":{"description":"Unauthorized"},"403":{"description":"Forbidden"},"404":{"description":"Not Found"}},"deprecated":false}},"/game/{id}/solution":{"post":{"tags":["game-controller"],"summary":"showGameSolution","operationId":"showGameSolutionUsingPOST","consumes":["application/json"],"produces":["*/*"],"parameters":[{"in":"body","name":"gameRequest","description":"gameRequest","required":true,"schema":{"$ref":"#/definitions/GameRequestDto"}},{"name":"id","in":"path","description":"id","required":true,"type":"integer","format":"int64"}],"responses":{"200":{"description":"OK","schema":{"$ref":"#/definitions/GameResponseDto"}},"201":{"description":"Created"},"401":{"description":"Unauthorized"},"403":{"description":"Forbidden"},"404":{"description":"Not Found"}},"deprecated":false}}},"definitions":{"AttemptDto":{"type":"object","properties":{"requestDate":{"type":"string","format":"date-time"},"userSolution":{"type":"string"}},"title":"AttemptDto"},"GameRequestDto":{"type":"object","properties":{"userAttempt":{"type":"string"},"userName":{"type":"string"}},"title":"GameRequestDto"},"GameResponseDto":{"type":"object","properties":{"attempts":{"type":"array","items":{"$ref":"#/definitions/AttemptDto"}},"currentGame":{"type":"string"},"gameKey":{"type":"string"},"gameResult":{"$ref":"#/definitions/GameResult"},"id":{"type":"string"},"possibilities":{"type":"array","items":{"type":"string","enum":["G","B","Y","R","O","W"]}}},"title":"GameResponseDto"},"GameResult":{"type":"object","properties":{"attempts":{"type":"integer","format":"int32"},"blacks":{"type":"integer","format":"int32"},"solved":{"type":"boolean"},"whites":{"type":"integer","format":"int32"}},"title":"GameResult"}}}


Enjoy the game!           
    
#####################################################################
